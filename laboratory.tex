%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                  H E A D E R                                 %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{header}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                D O C U M E N T                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\def\psep{4.0cm}
\def\qsep{3.0cm}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                                                              %
%                                   T I T L E                                  %
%                                                                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
{  \huge \bf \color{red} Laboratory XII  }\\[2mm]
{  \Large \textbf{\textit{\color{red} Density \& Archimedes' Principle }}  }\\[2mm]
{  \large \it \color{azure(colorwheel)} Introductory Physics I --- Physics 141L/171L  }
\end{center}
\hrule
\bigskip
\begin{tabularx}{\textwidth}{lXr}
%
	&
	{\bf Name:}
	&
	{\bf Laboratory Section:}
	\hspace{1.0cm}
	\\[2mm]
%
	&
	{\bf Partners:}\hspace{5.0cm}
\end{tabularx}
\medskip


%\def\SHOWSOLUTIONS{1}
%\newcommand\solution[1]{{\ifdefined\SHOWSOLUTIONS\color{richelectricblue}#1\fi}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                            I N T R O D U C T I O N                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Introduction}}

	The goal of this experiment is to measure the density of solid objects using
	three methods --- measurement of dimensions, immersion into a liquid
	and exploiting the Archimedes' Principle

\bigskip\noindent
	\emph{\textbf{Measurement method}}\\
	For regular--shaped objects, the relevant measurements of the object are made,
	and the volume of the object is calculated using standard volume equations.
	The mass is determined with a triple beam balance

\bigskip\noindent
	\emph{\textbf{Immersion method}}\\
	For irregularly shaped objects, determination of volume by the first method may not be practical,
	but the volume can easily be determined by displacement.
	Submerge the object in a graduated cylinder with a known volume of water.
	Note the volume with the object submerged --- the difference between the two volumes will be the volume of the object.
	The mass is again determined with a triple beam balance

\bigskip\noindent
	\emph{\textbf{Archimedes' Principle}}\\
	A body is weighed in air and then weighed when submerged in a liquid.
	The apparent loss of weight is, by Archimedes' Principle, equal to the weight of the liquid
	displaced by the body.
	From these measurements, the density and specific gravity of the solids and liquids used in the experiment
	may be determined


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                  T H E O R Y                                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Theory}}

	The fact that an object immersed in a \emph{fluid} --- liquid or gas, should be ``buoyed up''
	by a force equal to the weight of the fluid it displaces was deduced by Archimedes (287--212 BC).
	This principle, called Archimedes' Principle, applies to any object in any fluid ---
	for example, a submarine in water or a dirigible in air.
	If a free body remains at rest when totally immersed in a fluid, there must be no resultant force
	acting, and, hence, the weight of the body must be equal to the weight of the displaced fluid.
	The body will sink if its weight exceeds that of the displaced fluid and, conversely,
	rise if lighter.
	Thus a block of cork released in water, bobs up to the surface and floats like a ship.
	A ship will adjust to a depth in water for which its weight just equals the weight of water (and air)
	displaced.
	The contribution of the air to the buoyant force of a ship is negligible, and therefore, disregarded.
	The dirigible is, however, totally supported by the buoyant force supplied by air displacement.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          P R O O F   O F   A R C H I M E D E S '   P R I N C I P L E         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\textit{Proof of Archimedes' Principle}}
\begin{wrapfigure}{l}{0.28\textwidth}
\begin{center}
\begin{tikzpicture}
	% fix the random seed to always produce the same shape
	\pgfmathsetseed{23120}

	% fluid
	\draw [draw = none, fill = columbiablue, decoration = {random steps, segment length = 3mm, amplitude = 1.4mm}, rounded corners]
		decorate{(-2, 3.4)	--	(2, 3.4)}
					--	(2, 0)
					--	(-2, 0)
					--	cycle;

	% container
	\draw [line width = 0.8mm, draw = cadetgrey, line cap = round, rounded corners]
		(-2, 4)	--	(-2, 0)	--	(2, 0)	--	(2, 4);

\begin{scope}[shift = {(0, 1.6)}]
	% object
	\draw [dash pattern = on 1.4mm off 1.2mm, ultra thick, draw = cornflowerblue,
		decoration = {random steps, segment length = 2.4mm, amplitude = 1.6mm}, decorate]
		(0, 0) circle [radius = 0.8cm];

	% forces
	\begin{scope}[line width = 0.8mm, line cap = round, ->, >={Stealth[length=2.7mm, width=2.4mm]}, draw = crimson!80]
		\draw (160: 1.8cm)	--	(160: 1cm);
		\draw (-140: 1.8cm)	--	(-144: 1cm);
		\draw (-68: 1.56cm)	--	(-70: 0.9cm);
		\draw (-18: 1.8cm)	--	(-15: 1cm);
		\draw (50: 1.8cm)	--	(46: 1cm);
		\draw (118: 1.8cm)	--	(114: 1cm);
	\end{scope}
\end{scope}
\end{tikzpicture}
\end{center}
\end{wrapfigure}
	Let the irregular outline contain any desired portion of a fluid at rest.
	The arrows are the representative forces acting against the bounding surface.
	Each force is perpendicular to its element of surface,
	and the resulting pressure has a magnitude dependent on the depth
	of the fluid at that point

\bigskip\noindent
	Since the fluid is at rest, there is no unbalanced force in any direction.
	The components of the forces pushing towards on the bounding surface
	must balance those pushing towards the left.
	The vertical components upward on the surface must support the downward
	forces on the surface plus the weight of the designated portion of fluid.
	That is, the resultant upward force \cc{ F_v } must equal the weight
	\cc{ F_g ~=~ m g } of the fluid contained inside this surface

\bigskip\noindent
	When this portion of fluid is replaced by a solid body of exactly the same shape,
	the pressure at every point is exactly the same as before.
	Hence, the fluid must exert the upward or buoyant force on the object immersed in it,
	a force, which is again just equal to the weight of the fluid displaced by the object.
	If the buoyant force is greater than the weight of the object,
	the object is lifted to the surface.
	If the weight of the object is greater than the buoyant force,
	the unbalanced downward force causes the object to sink like a rock in water.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    A P P L I C A T I O N   O F   A R C H I M E D E S '   P R I N C I P L E   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\textit{Application of Archimedes' Principle}}

	Archimedes' Principle is experimentally applied to obtain the density
\ccc
\beq
\label{density}
	\rho	~~=~~	\frac m V\ccb\,,
\stepcounter{equation}
\tag{\ccb\arabic{equation}}
\eeq
\ccb
	where \cc{ m } is the mass and \cc{ V } the volume of the object

\bigskip\noindent
	A body of weight \cc{ F_g ~=~ m g } has an apparent loss of weight
	when immersed in a fluid.
	This loss is equal to the weight of the volume of fluid displaced by the object
	\cc{ F_f ~=~ m_f g }.
	Hence, the apparent weight of the object \cc{ F_a ~=~ m_a g } submerged in the fluid
	is given by
\ccc
\beq
\label{apparent}
	F_a	~~=~~	F_g  ~~-~~ F_f	~~=~~	m\, g  ~~-~~  m_f\, g
	\ccb\,.
\stepcounter{equation}
\tag{\ccb\arabic{equation}}
\eeq
\ccb
\noindent
	Since, for any substance, \cc{ m ~=~ \rho\, V } (see Eq.~\eqref{density}),
	one may replace \cc{ m_f } of Eq.~\eqref{apparent} 
	with \cc{ \rho_f\, V },
	where \cc{ \rho_f } is the density of the fluid
	and \cc{ V } the volume displaced.
	But the volume of fluid displaced is also the volume of the submerged object
	\cc{ V ~=~ m / \rho }.
	Thus, \cc{ m_f ~=~ \rho_f\, (\, m / \rho \,) },
	and Eq.~\eqref{apparent} becomes
\ccc
\[
	F_a	~~=~~	m\, g  ~~-~~  \rho_f\, \Big(\, \frac m \rho \,\Big)\, g
	\ccb\,,
\]
\ccb
	or,
\ccc
\beq
\label{rho}
	F_a	~~=~~	F_g  ~~-~~  F_g\, \frac{\rho_f} \rho
	\ccb\qquad\qquad\Longrightarrow\qquad\qquad
	\ccc
	\rho	~=~	\frac{ F_g }{ F_g \,-\, F_a }\, \rho_f
	\ccb\,.
\stepcounter{equation}
\tag{\ccb\arabic{equation}}
\eeq
\ccb
\noindent
	If the density of the body to be measured is less than the density of the fluid,
	that is, \cc{ \rho ~<~ \rho_f },
	it is necessary to fasten a sinker to the body so that the two together will sink in the fluid.
	If this sinker is made a part of the weighing apparatus for all the readings,
	the value of \cc{ \rho } again may be computed from the two readings required
	for Eq.~\eqref{rho}.
	Note that the sign of \cc{ F_a } is now negative (which is why the body would surface on its own).

\bigskip\noindent
	Eq.~\eqref{rho} states, that
\[
	\text{density of object}	~~=~
		\frac{\text{weight of object}}{\text{loss of weight when submerged in fluid}}
		\times
		\text{density of the fluid}\,.
\]
	When the fluid used is water, the \emph{ratio} in this equation can be re--written as,
\[
	\text{\textit{sp. gr.}}		~~=~~
		\frac{\text{weight of object}}{\text{weight of equal volume of water}}\,,
\]
	where \textit{sp. gr.} is called the specific gravity of the substance.
	\emph{Mercury} has a specific gravity of \cc{ 13.6 } ---
	this means that a given volume of mercury is \cc{ 13.6 } times heavier than
	\emph{the same volume of water}.
	To obtain the density of mercury,
	its specific gravity must be multiplied by the density of water,
	\emph{i.e.} \cc{ \SI{1}{\gram\per\cubic\centi\metre} }
	or \cc{ \SI{1}{lb\per\cubic{ft}} }.
	Note that specific gravity is a ratio only and has no units.
	Density has the units of mass per volume.
	In the British system, the units of \cc{ \si{lb\per\cubic{ft}} }
	give a ``weight density''

\bigskip\noindent
	This experiment deals only with solids and liquids which have a very high density
	compared to air.
	Thus, the ``weight of the object'' is accepted as approximately its weight in the air

\bigskip\noindent
	The relative densities of liquids may be obtained by finding the weights
	of a given dense object in the various liquids.
	Suppose that the apparent loss of weight of the object immersed in a liquid
	of a density \cc{ \rho_{f1} } is \cc{ m_1 g},
	and when it is immersed in a liquid of density \cc{ \rho_{f2} }
	the apparent loss of weight is \cc{ m_2 g}.
	By Archimedes' Principle, \cc{ m_1 g ~=~ \rho_{f1} V g } and
	\cc{ m_2 g ~=~ \rho_{f2} V g },
	where \cc{ V } is the volume of the object.
	Hence,
\ccc
\[
	V	~~=~~	\frac{ m_1 }{ \rho_{f1} }	~~=~~	\frac{ m_2 }{ \rho_{f2} }
	\ccb\qquad\qquad\Longrightarrow\qquad\qquad
	\ccc
	\rho_{f1}	~~=~~	\rho_{f2}\, \frac{ m_1 }{ m_2 }\ccb\,.
\]
\ccb
	The ratio \cc{ m_1 / m_2 } may be measured directly from the corresponding weights.
	The value of \cc{ \rho_{ft1} } is known.
	It is common practice to use water as a comparison liquid since its density
	is very accurately known.
	For the purposes of this laboratory, it can be assumed to be \cc{ \SI{1}{\gram\per\cubic\centi\metre} }
\begin{table}[h!]
\caption{Density of water}
\begin{center}
\begin{tabular}{
	>{\centering\arraybackslash}m{0.33\textwidth}
	>{\centering\arraybackslash}m{0.33\textwidth}
	>{\centering\arraybackslash}m{0.33\textwidth}
}
%
\toprule
\noalign{\vskip 2mm}
%
		\textbf{Temperature (\cc{ \mathbf{\si{\celsius}} })}			&
		\textbf{Density (\cc{ \mathbf{\si{\gram\per\cubic\centi\metre}} })}	&
		\textbf{Density (\cc{ \mathbf{\si{lb\per\cubic{ft}}} })}
		\\[2mm]
\midrule
\noalign{\vskip 4mm}
%
		0		&	0.99987		&	\multirow{3}{*}{62.42}	\\[2mm]
%
		2		&	0.99998						\\[2mm]
%
		3.98		&	1.00000						\\[2mm]
%
\midrule
\noalign{\vskip 2mm}
%
		6		&	0.99997						\\[2mm]
%
		10		&	0.99973						\\[2mm]
%
		15		&	0.99913						\\[2mm]
\midrule
\noalign{\vskip 2mm}
%
		20		&	0.99823		&	\multirow{3}{*}{62.28}	\\[2mm]
%
		22		&	0.99780						\\[2mm]
%
		24		&	0.99732						\\[2mm]
\midrule
\noalign{\vskip 2mm}
%
		26		&	0.99861						\\[4mm]
\bottomrule
\end{tabular}
\end{center}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                               P R O C E D U R E                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Procedure}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                   T O O L S                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\textit{Tools}}

\begin{itemize}
\item
	trip scale on a supporting stand

\item
	set of masses

\item
	cylinder or block of wool having a specific gravity of less than \cc{ 1 }

\item
	irregular metal object

\item
	vernier caliper

\item
	liquids of different densities (a dense solution of salt water may be used)
\end{itemize}

\bigskip\noindent
	\emph{\textbf{Measurement method}}\\
\begin{itemize}
\item
	determine the volume of a regularly--shaped object by measurements and standard volume calculations

\item
	determine the mass using the balance beams

\item
	divide the mass by the volume to get the density
\end{itemize}

\bigskip\noindent
	\emph{\textbf{Immersion method}}\\
\begin{itemize}
\item
	determine the volume of an object by immersion

\item
	determine the mass with the balance beams

\item
	find the density
\end{itemize}

\bigskip\noindent
	\emph{\textbf{Archimedes' Principle}}\\
\begin{itemize}
\item
	weigh the object in the air

\item
	then weigh again when submerged

\item
	use those two values and the density of the liquid to determine the object's density
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                A N A L Y S I S                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Analysis}}

	Calculate the densities of several objects with the various methods.
	\textit{Use all three methods on at least one object}.
	How did your results compare to the reference standard for the materials you used?
	Which of the methods would you expect to have the lowest error?
	Why?
	Did your results match your expectations?


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      O P T I O N A L   Q U E S T I O N S                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\textit{Optional questions}}

\begin{enumerate}
\item
	Why is it important that no air bubbles adhere to the objects during measurements?

\item
	The specific gravity of gold is \cc{ 19.3 }.
	What is the mass in grams of a cubic centimetre of gold?
	What is the weight in pounds of a cubic foot of gold?

\item
	A block of metal having a density of \cc{ \SI{9.00}{\gram\per\cubic\centi\metre} }
	has an apparent weight of \cc{ \SI{180}\gram } in water and \cc{ \SI{135}\gram }
	when submerged in a liquid.
	What is the density of the liquid?

\item
	A block of wood and a block of lead of the same mass are weighed carefully on a trip scale
	using brass weights.
	Are the weights of these blocks the same?
	Explain

\item
	A submarine floats at rest \cc{ \SI{50}{ft} } deep in the water.
	Without using the propeller how can the captain make it surface?
	Sink?

\item
	A rock has a specific gravity of \cc{ 1.30 }.
	Professor can lift \cc{ \SI{120}{lb} }.
	How many cubic feet of this rock can he lift in the air?
	In water?

\item
	Oil having a density of \cc{ \SI{0.80}{\gram\per\cubic\centi\metre} }
	floats on water of density \cc{ \SI{1.0}{\gram\per\cubic\centi\metre } }.
	A solid object which has a specific gravity of \cc{ 0.90 } is dropped into the container.
	Locate its exact position at rest
\end{enumerate}


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                   L A B O R A T O R Y   E V A L U A T I O N                  %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Laboratory evaluation}}

	Please provide feedback on the following areas, comparing this laboratory to your previous labs.
	Please assign each of the listed categories a value in 1 ~--~ 5, with 5 being the best, 1 the worst.

\bigskip
\begin{itemize}
\item
	how much fun you had completing this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well the lab preparation period explained this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the amount of work required compared to the time allotted?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	your understanding of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the difficulty of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well this laboratory tied in with the lecture?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5
\end{itemize}

\bigskip\noindent
	Comments supporting or elaborating on your assessment can also be very helpful in improving the future laboratories


\end{document}
